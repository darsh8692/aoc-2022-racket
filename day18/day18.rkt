#lang racket/base
(provide part-01 part-02 main)
(require "lib/common.rkt"
         racket/match racket/set)

(define DIRS '((1 0 0) (-1 0 0) (0 1 0) (0 -1 0) (0 0 1) (0 0 -1)))
(define (point+ a b) (map + a b))
(define XMAX 0)
(define YMAX 0)
(define ZMAX 0)

(define (part-01 input)
  (define lava (read-lava input))
  (count-faces lava))

(define (count-faces lava)
  (for/sum ([c (in-hash-keys lava)])
    (- 6 (length (neighbours lava c)))))

(define (read-lava input)
  (define cubes (map integers input))
  (for/hash ([c (in-list cubes)])
    (match-define (list x y z) c)
    (set! XMAX (max x XMAX))
    (set! YMAX (max y YMAX))
    (set! ZMAX (max z ZMAX))
    (values c 'lava)))

(define (neighbours lava p)
  (for/list ([d (in-list DIRS)]
             #:do [(define p+ (point+ p d))]
             #:when (lava-ref lava p+))
    p+))

(define (lava-ref l p)
  (hash-ref l p #f))

(define (part-02 input)
  (define lava (read-lava input))
  (- (count-faces lava)
     (count-internal-faces lava)))

(define (count-internal-faces lava)
  (define external (mark-external (list -1 -1 -1) lava))
  (for*/fold ([seen (set)]
              [faces 0]
              #:result faces)
             ([c (in-hash-keys lava)]
             [d (in-list DIRS)]
             #:do [(define p+ (point+ c d))]
             #:unless (set-member? external p+)
             #:unless (set-member? seen p+)
             #:unless (lava-ref lava p+))
    (values (set-add seen p+)
            (+ faces (length (neighbours lava p+))))))

(define (out-of-bounds? p)
  (match-define (list x y z) p)
  (not (and (<= -1 x (add1 XMAX))
            (<= -1 y (add1 YMAX))
            (<= -1 z (add1 ZMAX)))))

(define (mark-external p lava [external (set p)])
  (for/fold ([external external])
            ([d (in-list DIRS)]
             #:do [(define p+ (point+ p d))]
             #:unless (set-member? external p+)
             #:unless (out-of-bounds? p+)
             #:unless (lava-ref lava p+))
    (mark-external p+ lava (set-add external p+))))

;; =========================================================
;; =========================================================
;; =========================================================
(define input (read-input))
(define sample-input (lines
#<<SAMPLE
2,2,2
1,2,2
3,2,2
2,1,2
2,3,2
2,2,1
2,2,3
2,2,4
2,2,6
1,2,5
3,2,5
2,1,5
2,3,5
SAMPLE
))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))
(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)
  (check-= (part-01 sample-input) 64 0)
  (check-= (part-02 sample-input) 58 0)

  ;; regression tests
  (check-= (part-01 input) 4580 0)
  (check-= (part-02 input) 2610 0)

  )
