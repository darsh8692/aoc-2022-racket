#lang racket/base

(provide part-01 part-02 main timed)

(require "lib/common.rkt"
         racket/string racket/match racket/list)

(struct dir (contents) #:mutable #:transparent)
(struct file (size) #:transparent)
(define root (list "/"))

(define (part-01 input)
  (define disk (run-script null (mount-disk) input))
  (sum-dirs disk 100000))

(define (mount-disk)
  (make-hash `((,root . ,(dir null)))))

(define (run-script path disk out)
  (if (null? out)
      disk
      (match (string-split (first out))
        [(list _ "cd" "/")   (run-script root disk (rest out))]
        [(list _ "cd" "..")  (run-script (rest path) disk (rest out))]
        [(list _ "cd" subdir) (run-script (cons subdir path) disk (rest out))]
        [(list _ "ls")       (parse-listing path disk (rest out))]
        [_ (error 'execute-command "command expected: ~a" (first out))])))

(define (parse-listing path disk out)
  (cond [(null? out) disk]
        [else
         (match (string-split (first out))
           [(cons "$" _)  (run-script path disk out)]
           [(list "dir" name)  (add-directory path disk name out)]
           [(list size name)  (add-file path disk (string->number size) name out)]
           [_ (error 'parse-listing "unknown listing ~a" (first out))])]))

(define (add-directory path disk name out)
  (define new-dir (dir null))
  (define t (hash-ref disk path))
  (set-dir-contents! t (cons new-dir (dir-contents t)))
  (hash-set! disk (cons name path) new-dir)
  (parse-listing path disk (rest out)))

(define (add-file path disk size name out)
  (define new-path (cons name path))
  (define new-file (file size))
  (define t (hash-ref disk path))
  (set-dir-contents! t (cons new-file (dir-contents t)))
  (hash-set! disk new-path new-file)
  (parse-listing path disk (rest out)))

(define (dir-size t)
  (match t
    [(dir contents) (apply + (map dir-size contents))]
    [(file size) size]))

(define (sum-dirs disk limit)
  (for/sum ([(_ v) (in-hash disk)]
            #:when (dir? v))
    (define s (dir-size v))
    (if (> s limit) 0 s)))

(define (part-02 input)
  (define disk (run-script null (mount-disk) input))
  (define used (dir-size (hash-ref disk (list "/"))))
  (define space-needed (- used 40000000))
  (for/fold ([smallest used])
            ([v (in-hash-values disk)]
             #:when (dir? v))
    (define size (dir-size v) )
    (if (>= size space-needed)
        (min size smallest)
        smallest)))
;; =========================================================
;; =========================================================
;; =========================================================

(define input (read-input))
(define sample-input-string
#<<SAMPLE
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k
SAMPLE
)
(define sample-input (lines sample-input-string))

(define (main [mode 'ignore])
  (case mode
    [(timed)
     (displayln (elapsed-ns/gc (part-01 input)))
     (displayln (elapsed-ns/gc (part-02 input)))]
    [(untimed)
     (displayln (part-01 input))
     (displayln (part-02 input))]
    [else (void)]))

(define (timed) (main 'timed))

(module+ main
  (let* ([args (current-command-line-arguments)])
    (cond [(zero? (vector-length args)) (main)]
          [else (main (string->symbol (vector-ref args 0)))])))

(module+ test
  (require rackunit)
  (check-= (part-01 sample-input) 95437 0)
  (check-= (part-02 sample-input) 24933642 0)
  ;; regression tests
  (check-= (part-01 input) 2031851 0)
  (check-= (part-02 input) 2568781 0)
  )
